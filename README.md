# Kubernetes CICD


# Lest use a Kubernetes playground at Katacoda
https://katacoda.com/courses/kubernetes/playground

## Deploy gitlab-runner into kubernetes
For this I'll use helm
https://github.com/helm/helm/blob/master/docs/quickstart.md

To install helm I'll use the easy and probably insecure way because I dont care about security :)
```
# curl -L https://git.io/get_helm.sh | bash 
```
Prepare helm
```
helm init
helm repo add gitlab https://charts.gitlab.io
```

Install runner
```
helm install --namespace gitlab --name gitlab-runner -f values.yaml gitlab/gitlab-runner
```